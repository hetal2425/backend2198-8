﻿using AutoMapper;
using EchoChat.DB;
using EchoChat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EchoChat.Repository
{
    public class ReportRepository : IReportRepository
    {
        IRepositoryBase _repositoryBase = null;
        private readonly IMapper _mapper;
        public ReportRepository(IRepositoryBase repositoryBase, IMapper mapper)
        {
            _repositoryBase = repositoryBase;
            _mapper = mapper;
        }

        public GroupReportHeading GetGroupReportHeading(int clientID)
        {
            try
            {
                var result = _repositoryBase._Context.usp_Report_GetGroupReportHeadingAsync(clientID);
                result.Wait();
                List<usp_Report_GetGroupReportHeadingResult> data = result.Result;
                return _mapper.Map<usp_Report_GetGroupReportHeadingResult, GroupReportHeading>(data[0]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public GroupReportDetailsModel GetGroupReportDetails(int clientID, string freeText, DateTime? fromDate, DateTime? toDate, int pageSize, int pageNo, int timeZoneOffset)
        {
            try
            {
                int? total = 0;
                List<usp_Report_GetGroupReportDetailsResult> data = _repositoryBase._Context.usp_Report_GetGroupReportDetailsAsync(clientID, freeText, fromDate, toDate, pageSize, pageNo, timeZoneOffset, ref total);
                List<GroupReportDetails> list = _mapper.Map<List<usp_Report_GetGroupReportDetailsResult>, List<GroupReportDetails>>(data);

                GroupReportDetailsModel model = new GroupReportDetailsModel();
                model.GroupReportDetails = list;
                model.Total = total ?? 0;
                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CampaignReportHeading> GetCampaignReportHeading(int groupID)
        {
            try
            {
                var result = _repositoryBase._Context.usp_Report_GetCampaignReportHeadingAsync(groupID);
                result.Wait();
                List<usp_Report_GetCampaignReportHeadingResult> data = result.Result;
                return _mapper.Map<List<usp_Report_GetCampaignReportHeadingResult>, List<CampaignReportHeading>>(data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CampaignReportDetailsModel GetCampaignReportDetails(int groupID, string freeText, DateTime? fromDate, DateTime? toDate, int pageSize, int pageNo, int timeZoneOffset)
        {
            try
            {
                int? total = 0;
                List<usp_Report_GetCampaignReportDetailsResult> data = _repositoryBase._Context.usp_Report_GetCampaignReportDetailsAsync(groupID, freeText, fromDate, toDate, pageSize, pageNo, timeZoneOffset, ref total);
                List<CampaignReportDetails> list = _mapper.Map<List<usp_Report_GetCampaignReportDetailsResult>, List<CampaignReportDetails>>(data);

                CampaignReportDetailsModel model = new CampaignReportDetailsModel();
                model.CampaignReportDetails = list;
                model.Total = total ?? 0;
                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ReadReportDetailsModel GetReadReportDetails(int noticeID, string freeText, int pageSize, int pageNo, int timeZoneOffset)
        {
            try
            {
                int? total = 0;
                List<usp_Report_GetReadReportDetailsResult> data = _repositoryBase._Context.usp_Report_GetReadReportDetailsAsync(noticeID, freeText, pageSize, pageNo, timeZoneOffset, ref total);
                List<ReadReportDetails> list = _mapper.Map<List<usp_Report_GetReadReportDetailsResult>, List<ReadReportDetails>>(data);

                ReadReportDetailsModel model = new ReadReportDetailsModel();
                model.ReadReportDetails = list;
                model.Total = total ?? 0;
                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ChannelVerificationModel GetChannelVerification(int clientID, int pageSize, int pageNo, string freeText)
        {
            try
            {
                int? total = 0;
                List<usp_Report_ChannelVerificationResult> data = _repositoryBase._Context.usp_Report_ChannelVerificationAsync(clientID, pageSize, pageNo, freeText, ref total);
                List<ChannelVerification> list = _mapper.Map<List<usp_Report_ChannelVerificationResult>, List<ChannelVerification>>(data);

                ChannelVerificationModel model = new ChannelVerificationModel();
                model.verifications = list;
                model.Total = total ?? 0;

                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool BlockNotice(int noticeID)
        {
            try
            {
                NoticeDetails noticeDetail = _repositoryBase.Get<NoticeDetails>(n => n.NoticeId == noticeID).FirstOrDefault();
                noticeDetail.IsActive = true;
                _repositoryBase.Update<NoticeDetails>(noticeDetail);
                return _repositoryBase.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ChannelSavingModel> GetChannelSavingReportDetails(int clientID, DateTime? fromDate, DateTime? toDate, int timeZoneOffset)
        {
            try
            {
                var result = _repositoryBase._Context.usp_Report_GetChannelSavingReportAsync(clientID, fromDate, toDate, timeZoneOffset);
                result.Wait();
                List<usp_Report_GetChannelSavingReportResult> data = result.Result;
                return _mapper.Map<List<usp_Report_GetChannelSavingReportResult>, List<ChannelSavingModel>>(data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ChannelInsightModel> GetChannelInsightReportDetails(int clientID, int groupID, int noticeID, DateTime? fromDate, DateTime? toDate, int timeSlot, int timeZoneOffset)
        {
            try
            {
                var result = _repositoryBase._Context.usp_Report_GetChannelInsightReportAsync(clientID, groupID, noticeID, fromDate, toDate, timeSlot, timeZoneOffset);
                result.Wait();
                List<usp_Report_GetChannelInsightReportResult> data = result.Result;
                return _mapper.Map<List<usp_Report_GetChannelInsightReportResult>, List<ChannelInsightModel>>(data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<WeekdaysEngagementModel> GetWeekdaysEngagementReportDetails(int clientID, DateTime? fromDate, DateTime? toDate, int dayType, int timeSlot, int timeZoneOffset)
        {
            try
            {
                var result = _repositoryBase._Context.usp_Report_GetWeekdaysEngagementReportAsync(clientID, fromDate, toDate, timeSlot, timeZoneOffset);
                result.Wait();
                List<usp_Report_GetWeekdaysEngagementReportResult> data = result.Result;
                return _mapper.Map<List<usp_Report_GetWeekdaysEngagementReportResult>, List<WeekdaysEngagementModel>>(data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<CampaignSummaryModel> GetCampaignSummaryReportDetails(int clientID, int groupID, int noticeID, DateTime? fromDate, DateTime? toDate, int timeZoneOffset)
        {
            try
            {
                var result = _repositoryBase._Context.usp_Report_GetCampaignSummaryReportAsync(clientID, groupID, noticeID, fromDate, toDate, timeZoneOffset);
                result.Wait();
                List<usp_Report_GetCampaignSummaryReportResult> data = result.Result;
                return _mapper.Map<List<usp_Report_GetCampaignSummaryReportResult>, List<CampaignSummaryModel>>(data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ChannelProgressModel> GetChannelProgressReportDetails(int clientID, int timeZoneOffset)
        {
            try
            {
                var result = _repositoryBase._Context.usp_Report_GetChannelProgressReportAsync(clientID, timeZoneOffset);
                result.Wait();
                List<usp_Report_GetChannelTrendsReportResult> data = result.Result;
                return _mapper.Map<List<usp_Report_GetChannelTrendsReportResult>, List<ChannelProgressModel>>(data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void AddUpdateChannelPrediction(ChannelPredictionModel channelPrediction)
        {
            try
            {
                _repositoryBase.ExecuteSqlCommand(string.Format("exec usp_Report_InsertUpdateChannelPrediction '{0}', '{1}', '{2}' ", channelPrediction.UserID, channelPrediction.Slot, channelPrediction.Value));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ChannelPredictionFileModel> GenerateChannelPredictionFile()
        {
            try
            {
                var result = _repositoryBase._Context.usp_Report_GetChannelPredictionDataAsync();
                result.Wait();
                List<usp_ChannelPredictionFileResult> data = result.Result;
                return _mapper.Map<List<usp_ChannelPredictionFileResult>, List<ChannelPredictionFileModel>>(data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ListOfChannelPrediction GetChannelPrediction(string slot, string search, int pageSize, int pageNo)
        {
            try
            {
                int? total = 0;
                string mostPrefferedChannel = string.Empty;
                string secondMostPrefferedChannel = string.Empty;
                string thirdMostPrefferedChannel = string.Empty;
                var result = _repositoryBase._Context.usp_GetChannelPredictionAsync(slot, search, pageSize, pageNo, ref total, ref mostPrefferedChannel, ref secondMostPrefferedChannel, ref thirdMostPrefferedChannel);
                List<usp_GetChannelPredictionResult> data = result;
                List<GetChannelPrediction> ChannelPredictionData = _mapper.Map<List<usp_GetChannelPredictionResult>, List<GetChannelPrediction>>(data);

                ListOfChannelPrediction channelPredictionList = new ListOfChannelPrediction();
                channelPredictionList.PredictionList = ChannelPredictionData;
                channelPredictionList.Total = total ?? 0;
                channelPredictionList.MostPrefferedChannel = mostPrefferedChannel;
                channelPredictionList.SecondMostPrefferedChannel = secondMostPrefferedChannel;
                channelPredictionList.ThirdMostPrefferedChannel = thirdMostPrefferedChannel;


                return channelPredictionList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}