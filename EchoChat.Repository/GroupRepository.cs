﻿using AutoMapper;
using EchoChat.Constants;
using EchoChat.DB;
using EchoChat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EchoChat.Repository
{
    public class GroupRepository : IGroupRepository
    {
        IRepositoryBase _repositoryBase = null;
        private readonly IMapper _mapper;
        public GroupRepository(IRepositoryBase repositoryBase, IMapper mapper)
        {
            _repositoryBase = repositoryBase;
            _mapper = mapper;
        }

        public GroupDTO GetGroupByID(int groupID)
        {
            try
            {
                Group g = _repositoryBase.Get<Group>(x => x.GroupId == groupID).FirstOrDefault();
                return _mapper.Map<Group, GroupDTO>(g);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public GroupDTO CreateUpdateGroup(GroupDTO group)
        {
            try
            {
                Group g = _mapper.Map<GroupDTO, Group>(group);

                if (group.GroupID > 0 & !group.IsPrivate ?? false)
                {
                    Group ug = _repositoryBase.Get<Group>(x => x.GroupId == group.GroupID).FirstOrDefault();

                    string oldName = new string(ug.Name);
                    ug.Name = group.Name;

                    _repositoryBase.Update<Group>(ug);

                    _repositoryBase.AddLog(LogType.Update, ModuleType.Group, g.GroupId, $"Group {oldName} renamed as {group.Name} successfully", group.CreatedBy);

                    _repositoryBase.SaveChanges();
                    return group;
                }
                else
                {
                    if (group.IsPrivate ?? false)
                    {
                        Group ugTo = _repositoryBase.Get<Group>(x => x.Name == group.Name).FirstOrDefault();
                        if (ugTo != null && !string.IsNullOrEmpty(ugTo.Name))
                        {
                            ugTo.IsDeleted = false;
                            ugTo.IsActive = true;
                            ugTo.CreatedDate = DateTime.UtcNow;
                            _repositoryBase.Update<Group>(ugTo);
                            _repositoryBase.SaveChanges();
                            return group;
                        }
                    }

                    g.IsActive = true;
                    g.IsDeleted = false;
                    g.CreatedDate = DateTime.UtcNow;
                    _repositoryBase.Add<Group>(g);
                    _repositoryBase.SaveChanges();

                    List l = new List();
                    l.ClientId = group.ClientID;
                    l.CreatedBy = group.CreatedBy;
                    l.IsActive = true;
                    l.IsDeleted = false;
                    l.IsPrivate = group.IsPrivate ?? false;
                    l.Name = group.Name + " Default List";
                    _repositoryBase.Add<List>(l);
                    _repositoryBase.SaveChanges();

                    UserList ul = new UserList();
                    ul.UserId = group.CreatedBy;
                    ul.ListId = l.ListId;
                    _repositoryBase.Add<UserList>(ul);

                    if (group.IsPrivate ?? false)
                    {
                        UserList ulTo = new UserList();
                        ulTo.UserId = Guid.Parse(g.Name);
                        ulTo.ListId = l.ListId;
                        _repositoryBase.Add<UserList>(ulTo);
                    }

                    GroupList gl = new GroupList();
                    gl.GroupId = g.GroupId;
                    gl.ListId = l.ListId;
                    _repositoryBase.Add<GroupList>(gl);

                    GroupUserAdmin gua = new GroupUserAdmin();
                    gua.GroupId = g.GroupId;
                    gua.UserId = group.CreatedBy;
                    gua.CreatedBy = group.CreatedBy;
                    _repositoryBase.Add<GroupUserAdmin>(gua);

                    _repositoryBase.SaveChanges();

                    group.GroupID = g.GroupId;

                    _repositoryBase.AddLog(LogType.Add, ModuleType.Group, g.GroupId, $"Group {group.Name} created successfully", group.CreatedBy, true);

                    return group;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public GroupDTO CreateInteGrationGroup(GroupDTO group, List<Guid> lstGUsers)
        {
            try
            {
                Group g = _mapper.Map<GroupDTO, Group>(group);

                g.IsActive = true;
                g.IsDeleted = false;
                g.CreatedDate = DateTime.UtcNow;
                _repositoryBase.Add<Group>(g);
                _repositoryBase.SaveChanges();

                List l = new List();
                l.ClientId = group.ClientID;
                l.CreatedBy = group.CreatedBy;
                l.IsActive = true;
                l.IsDeleted = false;
                l.IsPrivate = false;
                l.Name = group.Name + " Integration List";
                _repositoryBase.Add<List>(l);
                _repositoryBase.SaveChanges();

                foreach (Guid groupUser in lstGUsers)
                {
                    UserList ul = new UserList();
                    ul.UserId = groupUser;
                    ul.ListId = l.ListId;
                    _repositoryBase.Add<UserList>(ul);
                }

                GroupList gl = new GroupList();
                gl.GroupId = g.GroupId;
                gl.ListId = l.ListId;
                _repositoryBase.Add<GroupList>(gl);

                GroupUserAdmin gua = new GroupUserAdmin();
                gua.GroupId = g.GroupId;
                gua.UserId = group.CreatedBy;
                gua.CreatedBy = group.CreatedBy;
                _repositoryBase.Add<GroupUserAdmin>(gua);

                _repositoryBase.SaveChanges();

                group.GroupID = g.GroupId;

                _repositoryBase.AddLog(LogType.Add, ModuleType.Group, g.GroupId, $"Integration Group {group.Name} created successfully", group.CreatedBy, true);

                return group;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteGroup(int groupID)
        {
            try
            {
                Group g = _repositoryBase.Get<Group>(x => x.GroupId == groupID).FirstOrDefault();
                g.IsDeleted = true;
                _repositoryBase.Update<Group>(g);
                _repositoryBase.AddLog(LogType.Delete, ModuleType.Group, groupID, $"Group {g.Name} deleted successfully", g.CreatedBy);
                return _repositoryBase.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UserChannelDataWithGroup GetGroupMappingUsers(string groupID, int parentID, string noticeDetails)
        {
            try
            {
                var result = _repositoryBase._Context.usp_GetGroupMappingUsersAsync(Convert.ToInt32(groupID), parentID, noticeDetails);
                result.Wait();
                List<usp_GetGroupMappingUsersResult> data = result.Result;
                List<UserChannelInfo> userConIds = _mapper.Map<List<usp_GetGroupMappingUsersResult>, List<UserChannelInfo>>(data);
                UserChannelDataWithGroup userConIdWithGroupName = new UserChannelDataWithGroup();
                userConIdWithGroupName.UserChannelInfoList = userConIds;

                string gName = _repositoryBase.Get<Group>(x => x.GroupId == Convert.ToInt32(groupID)).Select(g => g.Name).FirstOrDefault();
                userConIdWithGroupName.GroupName = gName;

                return userConIdWithGroupName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UserChannelInfo> GetUsersForPendingNotice(int noticeID, bool isSMS, bool isEmail, bool isWA)
        {
            try
            {
                var result = _repositoryBase._Context.usp_GetUsersForPendingNoticeAsync(noticeID, isSMS, isEmail, isWA);
                result.Wait();
                List<usp_GetUsersForPendingNoticeResult> data = result.Result;
                return _mapper.Map<List<usp_GetUsersForPendingNoticeResult>, List<UserChannelInfo>>(data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ListOrGroupWiseUsers GetMembersByGroupID(int groupID, int pageSize, int PageNo, string freeText, int timeZoneOffset)
        {
            try
            {
                int? total = 0;
                var result = _repositoryBase._Context.usp_User_SelectByGroupIDAsync(groupID, pageSize, PageNo, freeText, timeZoneOffset, ref total);
                List<usp_User_SelectByGroupIDResult> data = result;
                List<UserModel> userList = _mapper.Map<List<usp_User_SelectByGroupIDResult>, List<UserModel>>(data);

                ListOrGroupWiseUsers groupWiseUsers = new ListOrGroupWiseUsers();
                groupWiseUsers.UserList = userList;
                groupWiseUsers.Total = total ?? 0;

                return groupWiseUsers;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ListOrGroupWiseUsers GetMembersByListID(int listID, int pageSize, int PageNo, string freeText, int timeZoneOffset)
        {
            try
            {
                int? total = 0;
                var result = _repositoryBase._Context.usp_User_SelectByListIDAsync(listID, pageSize, PageNo, freeText, timeZoneOffset, ref total);
                List<usp_User_SelectByListIDResult> data = result;
                List<UserModel> userList = _mapper.Map<List<usp_User_SelectByListIDResult>, List<UserModel>>(data);

                ListOrGroupWiseUsers groupWiseUsers = new ListOrGroupWiseUsers();
                groupWiseUsers.UserList = userList;
                groupWiseUsers.Total = total ?? 0;

                return groupWiseUsers;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UserGroupModel GetUserGroups(Guid userID, string freeText, int pageSize, int PageNo, int timeZoneOffset)
        {
            try
            {
                int? total = 0;
                List<usp_GetUserGroupsResult> data = _repositoryBase._Context.usp_GetUserGroupsAsync(userID, freeText, pageSize, PageNo, timeZoneOffset, ref total);
                List<UserGroups> list = _mapper.Map<List<usp_GetUserGroupsResult>, List<UserGroups>>(data);

                UserGroupModel userGroupModel = new UserGroupModel();
                userGroupModel.UserGroups = list;
                userGroupModel.Total = total ?? 0;

                return userGroupModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }








        public bool CreateUpdateList(ListDTO list)
        {
            try
            {
                List l = _mapper.Map<ListDTO, List>(list);

                if (list.ListID > 0)
                {
                    List ul = _repositoryBase.Get<List>(x => x.ListId == list.ListID).FirstOrDefault();
                    string oldName = new string(list.Name);
                    ul.Name = list.Name;
                    _repositoryBase.Update<List>(ul);
                    _repositoryBase.AddLog(LogType.Update, ModuleType.List, list.ListID, $"List {oldName} renamed as {list.Name} successfully", list.CreatedBy, true);
                }
                else
                {
                    l.IsActive = true;
                    l.IsDeleted = false;
                    _repositoryBase.Add<List>(l);
                    this._repositoryBase._Context.SaveChanges();
                    _repositoryBase.AddLog(LogType.Add, ModuleType.List, l.ListId, $"List {list.Name} created successfully", list.CreatedBy, true);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteList(int listID)
        {
            try
            {
                List l = _repositoryBase.Get<List>(x => x.ListId == listID).FirstOrDefault();
                l.IsDeleted = true;
                _repositoryBase.Update<List>(l);
                _repositoryBase.AddLog(LogType.Delete, ModuleType.List, listID, $"List {l.Name} deleted successfully", l.CreatedBy);
                return _repositoryBase.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SetGroupListMapping(GroupListMapping mapping)
        {
            try
            {
                //Delete old mappings

                _repositoryBase.ExecuteSqlCommand(string.Format(" delete from GroupList where GroupID = {0}", mapping.GroupID));
                _repositoryBase.SaveChanges();

                foreach (int l in mapping.ListIDs)
                {
                    GroupList gl = new GroupList();
                    gl.GroupId = mapping.GroupID;
                    gl.ListId = l;
                    _repositoryBase.Add<GroupList>(gl);
                }

                return _repositoryBase.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ListDTO> GetAllListByClientID(int clientID, string freeText, int timeZoneOffset)
        {
            try
            {
                var result = _repositoryBase._Context.usp_GetListByClientIDAsync(clientID, freeText, timeZoneOffset);
                result.Wait();
                List<usp_GetListByClientIDResult> data = result.Result;
                return _mapper.Map<List<usp_GetListByClientIDResult>, List<ListDTO>>(data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<GroupDTO> GetAllGroupsByClientID(int clientID)
        {
            try
            {
                List<Group> Groups = _repositoryBase.Get<Group>(x => x.ClientId == clientID && x.IsDeleted == false).ToList();
                return _mapper.Map<List<Group>, List<GroupDTO>>(Groups);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ListSmallDTO> GetAllListByGroupID(int groupID)
        {
            try
            {
                List<ListSmallDTO> lists = (from gl in _repositoryBase.Get<GroupList>(g => g.GroupId == groupID)
                                            join l in
                                            _repositoryBase.Get<List>() on gl.ListId equals l.ListId
                                            select new ListSmallDTO()
                                            {
                                                ListID = l.ListId,
                                                Name = l.Name
                                            }).ToList();

                return lists;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool CheckGroupAlreadyExists(int groupID, string name)
        {
            try
            {
                if (groupID > 0)
                {
                    return _repositoryBase.Get<Group>(g => g.Name == name && g.GroupId != groupID).Any();
                }
                else
                {
                    return _repositoryBase.Get<Group>(g => g.Name == name).Any();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool CheckListAlreadyExists(int listID, string name)
        {
            try
            {
                if (listID > 0)
                {
                    return _repositoryBase.Get<List>(l => l.Name == name && l.ListId != listID).Any();
                }
                else
                {
                    return _repositoryBase.Get<List>(l => l.Name == name).Any();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int CheckIntegrationGroupAlreadyExists(string name)
        {
            try
            {
                return _repositoryBase.Get<Group>(g => g.Name == name).Select(g => g.GroupId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public GroupDTO CreatePrivateGroup(GroupDTO group, List<Guid> lstGUsers)
        {
            try
            {
                Group g = _mapper.Map<GroupDTO, Group>(group);

                g.IsActive = true;
                g.IsDeleted = false;
                g.CreatedDate = DateTime.UtcNow;
                _repositoryBase.Add<Group>(g);
                _repositoryBase.SaveChanges();

                List l = new List();
                l.ClientId = group.ClientID;
                l.CreatedBy = group.CreatedBy;
                l.IsActive = true;
                l.IsDeleted = false;
                l.IsPrivate = true;
                l.Name = group.Name + " Private List";
                _repositoryBase.Add<List>(l);
                _repositoryBase.SaveChanges();

                foreach (Guid groupUser in lstGUsers)
                {
                    UserList ul = new UserList();
                    ul.UserId = groupUser;
                    ul.ListId = l.ListId;
                    _repositoryBase.Add<UserList>(ul);
                }

                GroupList gl = new GroupList();
                gl.GroupId = g.GroupId;
                gl.ListId = l.ListId;
                _repositoryBase.Add<GroupList>(gl);

                GroupUserAdmin gua = new GroupUserAdmin();
                gua.GroupId = g.GroupId;
                gua.UserId = group.CreatedBy;
                gua.CreatedBy = group.CreatedBy;
                _repositoryBase.Add<GroupUserAdmin>(gua);

                group.GroupID = g.GroupId;

                _repositoryBase.AddLog(LogType.Add, ModuleType.Group, g.GroupId, $"Private Group {group.Name} created successfully", group.CreatedBy, true);

                _repositoryBase.SaveChanges();
                return group;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int CheckPrivateGroupAlreadyExists(string name, string mobileNo)
        {
            try
            {
                return _repositoryBase.Get<Group>(g => g.Name == name && g.Description == mobileNo && g.IsDeleted == false).Select(g => g.GroupId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<DNDUserModel> GetAllMembersByListID(int listID)
        {
            try
            {
                List<DNDUserModel> lists = (from ul in _repositoryBase.Get<UserList>(l => l.ListId == listID)
                                            join u in _repositoryBase.Get<User>() on ul.UserId equals u.UserId
                                            select new DNDUserModel()
                                            {
                                                UserID = u.UserId,
                                                MobileNo = u.MobileNo
                                            }).ToList();

                return lists;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool SaveDNDStatus(Guid userID, List<DNDStatusModel> listDNDStatus)
        {
            try
            {
                foreach (DNDStatusModel user in listDNDStatus)
                {
                    UserDnd _user = new UserDnd();
                    _user.UserId = user.UserID;
                    _user.Dndstatus = user.Status;
                    _user.CreatedBy = userID;
                    _user.CreatedDate = DateTime.UtcNow;
                    _repositoryBase.Add<UserDnd>(_user);
                }
                return _repositoryBase.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DNDReportDetailsModel GetDNDReoprt(int clientID, int pageSize, int pageNo, string freeText, int timeZoneOffset)
        {
            try
            {
                int? total = 0;
                List<usp_Report_GetDNDReportDetailsResult> data = _repositoryBase._Context.usp_Report_GetDNDReoprtDetailsAsync(clientID, pageSize, pageNo, freeText, timeZoneOffset, ref total);
                List<DNDReportDetails> list = _mapper.Map<List<usp_Report_GetDNDReportDetailsResult>, List<DNDReportDetails>>(data);
               
                DNDReportDetailsModel model = new DNDReportDetailsModel();
                model.DNDReportDetails = list;
                model.Total = total ?? 0;
                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
