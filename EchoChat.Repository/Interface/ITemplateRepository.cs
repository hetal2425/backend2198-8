﻿using EchoChat.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EchoChat.Repository
{
    public interface ITemplateRepository
    {
        void AddTemplate(TemplateDTO template);
        ListOfTemplates GetTemplate(string title, string type, int pageSize, int pageNo);
        void RemoveTemplate(int id);
        void UpdateTemplate(TemplateDTO template);
        bool CheckTemplateExist(int id);
        bool CheckTemplateExistByName(string name);
    }
}
