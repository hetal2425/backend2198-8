﻿using AutoMapper;
using EchoChat.Constants;
using EchoChat.DB;
using EchoChat.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EchoChat.Repository
{
    public class AuthRepository : IAuthRepository
    {
        IRepositoryBase _repositoryBase = null;
        private readonly IMapper _mapper;
        public AuthRepository(IRepositoryBase repositoryBase, IMapper mapper)
        {
            _repositoryBase = repositoryBase;
            _mapper = mapper;
        }

        public UserData CheckUserLogin(LoginRequest user)
        {
            try
            {
                var result = _repositoryBase._Context.usp_GetUserLoginAsync(user.UserName, user.Password);
                result.Wait();
                List<usp_GetUserLoginResult> data = result.Result;
                if (data != null && data.Count > 0 && data[0] != null)
                {
                    _repositoryBase.AddLog(LogType.Login, ModuleType.User, 0, $"User {data[0].UserName} Logged in successfully", data[0].UserID);
                    return _mapper.Map<usp_GetUserLoginResult, UserData>(data[0]);
                }
                else
                {
                    return new UserData() { UserName = string.Empty };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
