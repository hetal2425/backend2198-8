﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EchoChat.Models.WebHookModels
{
    public class WAMessages
    {
        public List<WAMessage> messages { get; set; }
    }

    public class WAStatus
    {
        public List<Status> statuses { get; set; }
    }
    public class Status
    {
        public List<Error> errors { get; set; }
        public string id { get; set; }
        public string recipient_id { get; set; }
        public string status { get; set; }
        public string timestamp { get; set; }
    }

    public class Error
    {
        public int code { get; set; }
        public string title { get; set; }
    }
    public class Text
    {
        public string body { get; set; }
    }

    public class WAMessage
    {
        public string from { get; set; }
        public string id { get; set; }
        public Text text { get; set; }
        public string timestamp { get; set; }
        public DateTime Date { get; set; }
        public string type { get; set; }
    }
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class AppDetails
    {
        public string type { get; set; }
        public string id { get; set; }
    }

    public class Recipient
    {
        public string to { get; set; }
        public string recipient_type { get; set; }
    }

    public class Sender
    {
        public string from { get; set; }
    }

    public class Events
    {
        public string eventType { get; set; }
        public string timestamp { get; set; }
        public string date { get; set; }
        public string mid { get; set; }
    }

    public class NotificationAttributes
    {
        public string status { get; set; }
        public string reason { get; set; }
        public string code { get; set; }
    }

    public class WASMessageKarix
    {
        public string channel { get; set; }
        public AppDetails appDetails { get; set; }
        public Recipient recipient { get; set; }
        public Sender sender { get; set; }
        public Events events { get; set; }
        public NotificationAttributes notificationAttributes { get; set; }
    }

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Message
    {
        public string from { get; set; }
        public string id { get; set; }
        public Text text { get; set; }
        public string to { get; set; }
        public string contentType { get; set; }
    }

    public class EventContent
    {
        public Message message { get; set; }
    }

    public class WARMessageKarix
    {
        public string channel { get; set; }
        public AppDetails appDetails { get; set; }
        public Events events { get; set; }
        public EventContent eventContent { get; set; }
    }
}