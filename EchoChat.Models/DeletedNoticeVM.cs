﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoChat.Models
{
    public class DeletedNotice
    {
        public string DeviceToken { get; set; }
        public string Platform { get; set; }
    }
}
