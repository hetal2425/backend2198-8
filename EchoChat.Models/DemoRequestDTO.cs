﻿using System;
using System.Collections.Generic;

namespace EchoChat.Models
{
    public partial class DemoRequestDTO
    {
        public string Name { get; set; }
        public string MobileNumber { get; set; }
        public string EmailID { get; set; }
        public string Remark { get; set; }
        public string PageSource { get; set; }
        public string Org { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
    }
}
