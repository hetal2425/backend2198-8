﻿using System;
using System.Collections.Generic;

namespace EchoChat.Models
{
    public class NoticeDetailDTO
    {
        public int NoticeID { get; set; }
        public string NoticeTitle { get; set; }
        public string NoticeDetail { get; set; }
        public string NoticeDetailHTML { get; set; }
        public DateTime NoticeDate { get; set; }
        public int? GroupID { get; set; }
        public int? ParentID { get; set; }
        public Guid CreatedBy { get; set; }
        public string FileName { get; set; }
        public int? FileType { get; set; }
        public bool? IsReply { get; set; }
        public string LatestMessage { get; set; }
        public bool? IsSms { get; set; }
        public bool? IsEmail { get; set; }
        public bool? IsWhatsapp { get; set; }
        public bool? IsFacebook { get; set; }
        public bool? IsViber { get; set; }
        public bool? IsTelegram { get; set; }
        public bool? IsSlack { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }

        public DateTime? Date { get; set; }
        public TimeSpan? Time { get; set; }
        public DateTime? EndDate { get; set; }
        public bool? IsRecursive { get; set; }
        public string Days { get; set; }
    }
}
