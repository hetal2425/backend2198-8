﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoChat.Models
{
    public class UpdatePassword
    {
        public string UserID { get; set; }
        public string Password { get; set; }
        public string RecoveryNo { get; set; }
    }
}
