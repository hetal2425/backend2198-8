﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoChat.Models
{
    public class NoticeReport
    {
        public string FirstName { get; set; }
        public bool IsAdmin { get; set; }
        public string CreateDate { get; set; }
        public string AppReadDate { get; set; }
        public string MsgId { get; set; }
        public int WaId { get; set; }
        public string Status { get; set; }
        public string Timestamp { get; set; }
    }
}
