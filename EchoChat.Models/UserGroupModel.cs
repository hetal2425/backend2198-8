﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EchoChat.Models
{
    public class UserGroupModel
    {
        public int Total { get; set; }
        public List<UserGroups> UserGroups { get; set; }
    }

    public class UserGroups
    {
        public int GroupID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? TotalMembers { get; set; }
        public string LastRead { get; set; }
        public int ReplyReadPendingCount { get; set; }
        public bool? IsPrivate { get; set; }
    }
}
