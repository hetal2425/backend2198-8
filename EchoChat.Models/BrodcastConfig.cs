﻿using System;
using System.Collections.Generic;

namespace EchoChat.Models
{
    public class BrodcastConfig
    {
        public int ID { get; set; }
        public string NoticeDetail { get; set; }
        public DateTime NoticeDate { get; set; }
        public string SenderName { get; set; }
        public string NoticeTitle { get; set; }
        public bool? IsSms { get; set; }
        public bool? IsEmail { get; set; }
        public bool? IsWhatsapp { get; set; }
        public bool? IsFacebook { get; set; }
        public string origintorMail { get; set; }
        public string GroupName { get; set; }

        public string DeviceId { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public int? FileType { get; set; }
        public int ClientID { get; set; }
        public List<AppSettingsVM> appSettings { get; set; }
    }




}

