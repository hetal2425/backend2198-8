﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Amazon;
using Amazon.SimpleEmail;
using EchoChat.ChannelModels;
using Newtonsoft.Json;

namespace EchoChat.ChannelVerification
{
    public static class MobileVerification
    {
        public static bool VerifyMobile(string mobleNo)
        {

            try
            {
                using var client = new HttpClient();
                var data = client.GetAsync($"https://peoplestacks.com/api/v1/lookup/people/sprout/+91{mobleNo}");
                data.Wait();
                if(data.Result.StatusCode == HttpStatusCode.OK)
                {
                    var model = data.Result.Content.ReadAsStringAsync();
                    model.Wait();
                    MobileVerificationModel output = JsonConvert.DeserializeObject<MobileVerificationModel>(model.Result.ToString());
                    return output.data.number_details.is_valid;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public class CallerName
    {
        public string caller_name { get; set; }
        public int error_code { get; set; }
    }

    public class Formats
    {
        public string E164 { get; set; }
        public string national { get; set; }
        public string international { get; set; }
    }

    public class NumberDetails
    {
        public bool is_valid { get; set; }
        public Formats formats { get; set; }
        public string number { get; set; }
        public int calling_code { get; set; }
        public string country_code { get; set; }
        public string location { get; set; }
        public string number_type { get; set; }
        public string carrier { get; set; }
        public List<string> time_zone { get; set; }
    }

    public class Data
    {
        public string cuid { get; set; }
        public string key { get; set; }
        public string md5_hash { get; set; }
        public CallerName caller_name { get; set; }
        public NumberDetails number_details { get; set; }
    }

    public class MobileVerificationModel
    {
        public Data data { get; set; }
    }
}
