backports.entry-points-selectable==1.1.0
boto3==1.18.41
botocore==1.21.41
click==8.0.1
colorama==0.4.4
cycler==0.10.0
distlib==0.3.2
et-xmlfile==1.1.0
filelock==3.0.12
Flask==2.0.1
Flask-SQLAlchemy==2.5.1
greenlet==1.1.1
HTMLParser==0.0.2
itsdangerous==2.0.1
Jinja2==3.0.1
jmespath==0.10.0
joblib==1.0.1
kiwisolver==1.3.2
Mako==1.1.5
MarkupSafe==2.0.1
matplotlib==3.4.3
numpy==1.21.2
openpyxl==3.0.8
pandas==1.3.3
Pillow==8.3.2
platformdirs==2.3.0
psycopg2==2.9.1
pymongo==3.12.0
pyparsing==2.4.7
python-dateutil==2.8.2
pytz==2021.1
s3transfer==0.5.0
scikit-learn==0.24.2
scipy==1.7.1
seaborn==0.11.2
six==1.16.0
sklearn==0.0
SQLAlchemy==1.4.23
threadpoolctl==2.2.0
urllib3==1.26.6
virtualenv==20.7.2
Werkzeug==2.0.1
xgboost==1.4.2
