﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class VwChannelReadAndDeliveryPer
    {
        public int NoticeId { get; set; }
        public int? GroupId { get; set; }
        public string Channel { get; set; }
        public int? DratePer { get; set; }
        public int? RratePer { get; set; }
    }
}
