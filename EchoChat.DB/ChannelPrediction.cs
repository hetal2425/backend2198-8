﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class ChannelPrediction
    {
        public int Id { get; set; }
        public Guid? UserId { get; set; }
        public string Mo246 { get; set; }
        public string Mo612 { get; set; }
        public string Mo1218 { get; set; }
        public string Mo1824 { get; set; }
        public string Tu246 { get; set; }
        public string Tu612 { get; set; }
        public string Tu1218 { get; set; }
        public string Tu1824 { get; set; }
        public string We246 { get; set; }
        public string We612 { get; set; }
        public string We1218 { get; set; }
        public string We1824 { get; set; }
        public string Th246 { get; set; }
        public string Th612 { get; set; }
        public string Th1218 { get; set; }
        public string Th1824 { get; set; }
        public string Fr246 { get; set; }
        public string Fr612 { get; set; }
        public string Fr1218 { get; set; }
        public string Fr1824 { get; set; }
        public string Sa246 { get; set; }
        public string Sa612 { get; set; }
        public string Sa1218 { get; set; }
        public string Sa1824 { get; set; }
        public string Su246 { get; set; }
        public string Su612 { get; set; }
        public string Su1218 { get; set; }
        public string Su1824 { get; set; }
    }
}
