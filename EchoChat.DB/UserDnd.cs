﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class UserDnd
    {
        public int Id { get; set; }
        public Guid? UserId { get; set; }
        public string Dndstatus { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
