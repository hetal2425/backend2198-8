﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class Client
    {
        public int Id { get; set; }
        public string OrganizationName { get; set; }
        public string ContactPersonName { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public DateTime? CreateDate { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsTrial { get; set; }
        public string ClientLogo { get; set; }
    }
}
