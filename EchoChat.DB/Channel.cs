﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class Channel
    {
        public int ChannelId { get; set; }
        public string Name { get; set; }
        public bool? IsActive { get; set; }
    }
}
