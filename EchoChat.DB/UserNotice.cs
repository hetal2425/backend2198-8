﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class UserNotice
    {
        public int UserNoticeId { get; set; }
        public int NoticeId { get; set; }
        public int GroupId { get; set; }
        public Guid? UserId { get; set; }
        public bool? IsRead { get; set; }
        public bool? IsNotified { get; set; }
        public DateTime? CreateDate { get; set; }
        public string WamsgId { get; set; }
        public string EmailMsgId { get; set; }
        public string SmsmsgId { get; set; }
        public DateTime? AppReadDate { get; set; }
        public DateTime? AppNotifyDate { get; set; }
        public string SlackMsgId { get; set; }
    }
}
