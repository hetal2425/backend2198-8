﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class User
    {
        public Guid UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MobileNo { get; set; }
        public string EmailId { get; set; }
        public DateTime? Dob { get; set; }
        public string Occupation { get; set; }
        public string Password { get; set; }
        public bool? IsPasswordUpdated { get; set; }
        public DateTime? CreateDate { get; set; }
        public string ProfilePhoto { get; set; }
        public string Session { get; set; }
        public int ClientId { get; set; }
        public string FacebookId { get; set; }
        public string WhatsAppId { get; set; }
        public bool? IsDeleted { get; set; }
        public Guid? CreatedBy { get; set; }
        public bool? IsOnWhatsApp { get; set; }
        public bool? IsOnSms { get; set; }
        public bool? IsOnEmail { get; set; }
        public bool? IsOnTelegram { get; set; }
        public bool? IsIntroHappened { get; set; }
        public bool? IsOnEcho { get; set; }
        public bool? IsOnEchoMobileApp { get; set; }
        public bool? IsOnPushNoti { get; set; }
        public string SlackId { get; set; }
        public bool? IsOnSlack { get; set; }
    }
}
