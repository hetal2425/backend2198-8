﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class ChannelDatum
    {
        public int ChannelDataId { get; set; }
        public string UserId { get; set; }
        public string MsgId { get; set; }
        public string Status { get; set; }
        public string MsgType { get; set; }
        public DateTime? Timestamp { get; set; }
    }
}
