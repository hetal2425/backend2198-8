﻿using EchoChat.Models;
using EchoChat.ServiceProvider;
using EchoChatApp.CronJobServices.CronJobExtensionMethods;
using EchoChatApp.General;
using EchoChatApp.Helper;
using log4net;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace EchoChatApp.CronJobServices
{
    public class ScheduleChannelPredictionFileSynced : CronJobService, IDisposable
    {
        private readonly IServiceScope _scope;
        private readonly AppSettingsFromFile settingsFromFile;
        private ILog logger;
        public ScheduleChannelPredictionFileSynced(IOptions<AppSettingsFromFile> oSettingsFromFile, IScheduleConfig<ScheduleMessageSynced> config, IServiceProvider scope) : base(config.CronExpression, config.TimeZoneInfo)
        {
            settingsFromFile = oSettingsFromFile.Value;
            _scope = scope.CreateScope();
            logger = Logger.GetLogger(typeof(ScheduleMessageSynced));
        }
        public override Task StartAsync(CancellationToken cancellationToken)
        {
            return base.StartAsync(cancellationToken);
        }
        public override Task DoWork(CancellationToken cancellationToken)
        {
            try
            {
                string Url = settingsFromFile.BaseAPI + "api/Dashboard/GenerateChannelPredictionFile";
                var client = new RestClient(Url);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                IRestResponse response = client.Execute(request);
            }
            catch (Exception ex)
            {
                logger.Error("Scheduler Exception => " + ex.Message);
            }
            return Task.CompletedTask;
        }
        public override Task StopAsync(CancellationToken cancellationToken)
        {
            return base.StopAsync(cancellationToken);
        }
        public override void Dispose()
        {
            _scope?.Dispose();
        }
    }
}
