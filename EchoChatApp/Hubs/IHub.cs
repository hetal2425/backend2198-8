﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoChatApp.Hubs
{
    public interface IHub
    {
        Task SendToCaller(string message);
    }
}
