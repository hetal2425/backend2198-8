namespace EchoChatApp.Helper
{
    public class AppSettingsFromFile
    {
        public string Secret { get; set; }
        public string BaseAPI { get; set; }
        public string[] AllowedOrigin { get; set; }
        public string WhatsAppProvider { get; set; }
    }
}