﻿using EchoChat.Channel;
using EchoChat.Models;
using EchoChat.ServiceProvider;
using EchoChatApp.General;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoChatApp.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TestController : ControllerBase
    {

        [Route("Show")]
        [HttpGet]
        public IActionResult Show()
        {
            try
            {
                return Ok("Production server is running....");
            }
            catch (Exception ex)
            {
                return BadRequest(new { Message = ex.Message });
            }
        }
    }
}