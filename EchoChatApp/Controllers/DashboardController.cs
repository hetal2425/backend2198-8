﻿using ClosedXML.Excel;
using EchoChat.Helper;
using EchoChat.Models;
using EchoChat.ServiceProvider;
using EchoChatApp.General;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EchoChatApp.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class DashboardController : BaseController
    {
        private INoticeProvider noticeProvider;
        private IUserProvider userProvider;
        private IReportProvider reportProvider;
        private ILog logger;
        public DashboardController(INoticeProvider oNoticeProvider, IUserProvider oUserProvider, IReportProvider oReportProvider)
        {
            noticeProvider = oNoticeProvider;
            userProvider = oUserProvider;
            reportProvider = oReportProvider;
            logger = Logger.GetLogger(typeof(DashboardController));
        }


        [HttpGet("GetGroupReportHeading")]
        public IActionResult GetGroupReportHeading()
        {
            try
            {
                GroupReportHeading result = reportProvider.GetGroupReportHeading(ClientID);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("GetGroupReportHeading: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("GetGroupReportDetails")]
        public IActionResult GetGroupReportDetails(int pageSize = 50, int pageNo = 1, string freeText = null, DateTime? fromDate = null, DateTime? toDate = null)
        {
            try
            {
                GroupReportDetailsModel result = reportProvider.GetGroupReportDetails(ClientID, freeText, fromDate, toDate, pageSize, pageNo, TimeZoneOffset);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("GetGroupReportDetails: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("GetCampaignReportHeading")]
        public IActionResult GetCampaignReportHeading(int groupID)
        {
            try
            {
                List<CampaignReportHeading> result = reportProvider.GetCampaignReportHeading(groupID);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("GetCampaignReportHeading: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("GetCampaignReportDetails")]
        public IActionResult GetCampaignReportDetails(int groupID, int pageSize = 50, int pageNo = 1, string freeText = null, DateTime? fromDate = null, DateTime? toDate = null)
        {
            try
            {
                CampaignReportDetailsModel result = reportProvider.GetCampaignReportDetails(groupID, freeText, fromDate, toDate, pageSize, pageNo, TimeZoneOffset);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("GetCampaignReportDetails: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("GetReadReportHeading")]
        public IActionResult GetReadReportHeading(int noticeID)
        {
            try
            {
                NoticeDetailDTO result = noticeProvider.GetNoticeDetailsByNoticeID(noticeID);
                UserProfile user = userProvider.GetUserByID(result.CreatedBy);

                ReadReportHeading readReportHeading = new ReadReportHeading();
                readReportHeading.NoticeTitle = result.NoticeTitle;
                readReportHeading.NoticeDate = result.NoticeDate.AddMinutes(TimeZoneOffset).ToString("dd MMM yyyy hh:mm tt");
                readReportHeading.Owner = user.FirstName;

                return Ok(readReportHeading);
            }
            catch (Exception ex)
            {
                logger.Error("GetReadReportHeading: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("GetReadReportDetails")]
        public IActionResult GetReadReportDetails(int noticeID, int pageSize = 50, int pageNo = 1, string freeText = null)
        {
            try
            {
                ReadReportDetailsModel result = reportProvider.GetReadReportDetails(noticeID, freeText, pageSize, pageNo, TimeZoneOffset);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("GetReadReportDetails: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("GetChannelVerification")]
        public IActionResult GetChannelVerification(int pageSize = 50, int pageNo = 1, string freeText = null)
        {
            try
            {
                ChannelVerificationModel result = reportProvider.GetChannelVerification(ClientID, pageSize, pageNo, freeText);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("GetChannelVerification: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("BlockNotice")]
        public IActionResult BlockNotice(int NoticeID)
        {
            try
            {
                reportProvider.BlockNotice(NoticeID);
                return Ok(new { Message = "SUCCESS" });
            }
            catch (Exception ex)
            {
                logger.Error("BlockNotice: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("GetChannelSavingReportDetails")]
        public IActionResult GetChannelSavingReportDetails(DateTime? fromDate = null, DateTime? toDate = null)
        {
            try
            {
                List<ChannelSavingModel> result = reportProvider.GetChannelSavingReportDetails(ClientID, fromDate, toDate, TimeZoneOffset);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("GetChannelSavingReportDetails: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("GetChannelInsightReportDetails")]
        public IActionResult GetChannelInsightReportDetails(int groupID, int noticeID, DateTime? fromDate = null, DateTime? toDate = null, int timeSlot = 0)
        {
            try
            {
                List<ChannelInsightModel> result = reportProvider.GetChannelInsightReportDetails(ClientID, groupID, noticeID, fromDate, toDate, timeSlot, TimeZoneOffset);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("GetChannelInsightReportDetails: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("GetWeekdaysEngagementReportDetails")]
        public IActionResult GetWeekdaysEngagementReportDetails(DateTime? fromDate = null, DateTime? toDate = null, int dayType = 0, int timeSlot = 0)
        {
            try
            {
                List<WeekdaysEngagementModel> result = reportProvider.GetWeekdaysEngagementReportDetails(ClientID, fromDate, toDate, dayType, timeSlot, TimeZoneOffset);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("GetWeekdaysEngagementReportDetails: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("GetCampaignSummaryReportDetails")]
        public IActionResult GetCampaignSummaryReportDetails(int groupID, int noticeID, DateTime? fromDate = null, DateTime? toDate = null)
        {
            try
            {
                List<CampaignSummaryModel> result = reportProvider.GetCampaignSummaryReportDetails(ClientID, groupID, noticeID, fromDate, toDate, TimeZoneOffset);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("GetCampaignSummaryReportDetails: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("GetChannelProgressReportDetails")]
        public IActionResult GetChannelProgressReportDetails()
        {
            try
            {
                List<ChannelProgressModel> result = reportProvider.GetChannelProgressReportDetails(ClientID, TimeZoneOffset);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("GetChannelProgressReportDetails: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("GetReadReportDetailsExcel")]
        public IActionResult GetReadReportDetailsExcel(int noticeID, int pageSize = 50, int pageNo = 1, string freeText = null)
        {
            try
            {
                ReadReportDetailsModel result = reportProvider.GetReadReportDetails(noticeID, freeText, pageSize, pageNo, TimeZoneOffset);
                var stream = ExcelProcessing.CreateExcel(result);
                string excelName = $"ReadReport_" + noticeID + ".xlsx";
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", excelName);
            }
            catch (Exception ex)
            {
                logger.Error("GetReadReportDetailsExcel: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpPost("AddUpdateChannelPrediction")]
        public IActionResult AddUpdateChannelPrediction(ChannelPredictionModel channelPrediction)
        {
            try
            {
                reportProvider.AddUpdateChannelPrediction(channelPrediction);
                return Ok(new { });
            }
            catch (Exception ex)
            {
                logger.Error("AddUpdateChannelPrediction: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpPost("GetChannelPrediction")]
        public IActionResult GetChannelPrediction(string slot, string search, int pageSize, int pageNo)
        {
            try
            {
                if (string.IsNullOrEmpty(slot))
                {
                    return BadRequest(new { Message = "Slot is required!" });
                }
                var channelPredictionData = reportProvider.GetChannelPrediction(slot, search, pageSize, pageNo);
                if (channelPredictionData != null)
                {
                    return Ok(channelPredictionData);
                }
                return Ok(true);
            }
            catch (Exception ex)
            {
                logger.Error("GetChannelPrediction" + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [AllowAnonymous]
        [HttpPost("GenerateChannelPredictionFile")]
        public async Task<IActionResult> GenerateChannelPredictionFileAsync()
        {
            try
            {
                DataTable dtFile = new DataTable();
                logger.Info("Start process to upload channel prediction file");
                var fileList = reportProvider.GenerateChannelPredictionFile();
                if (fileList != null)
                {
                    //Convert List to datatable
                    dtFile = ConvertListToDataTable(fileList);
                }

                //Replace long text from cell because excel file support only 32767 charecters in each cell
                if (dtFile.Rows.Count > 0)
                {
                    foreach (DataRow row in dtFile.Rows)
                    {
                        for (int i = 0; i < dtFile.Columns.Count; i++)
                        {
                            if (!Convert.IsDBNull(row[i]))
                            {
                                string value = row[i].ToString();
                                if (value.Length > 32767)
                                {
                                    value = value.Substring(0, 32750);
                                    row[i] = value;
                                }
                            }
                        }
                    }

                    //Convert Datatable to Excel file
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(dtFile);
                        using (MemoryStream stream = new MemoryStream())
                        {
                            wb.SaveAs(stream);
                            //var FileName = DateTime.UtcNow.ToString("mmddyyyyhhmmssfff") + "_Test.xlsx";
                            var FileName = "channelprediction.xlsx";
                            await AWSHelper.RemoveFileFromS3(FileName);
                            AWSHelper.UploadFileToS3(stream, null, FileName);
                            byte[] myfile = await new WebClient().DownloadDataTaskAsync(new Uri(AWSHelper.GetPresignedFileURL(FileName)));
                            //return File(myfile, "image/png", FileName);
                        }
                    }
                    logger.Info("End process to upload channel prediction file");

                    //logger.Info("Start Process to Convert from DataTable To Csv File");
                    //var csv = ConvertDataTableToCsv(dtFile);
                    //if (csv != null)
                    //{
                    //    //var FileName = DateTime.UtcNow.ToString("mmddyyyyhhmmssfff") + "_channelprediction.csv";
                    //    var FileName = "channelprediction.csv";
                    //    var myString = csv.ToString();
                    //    var myByteArray = Encoding.UTF8.GetBytes(myString);
                    //    var ms = new MemoryStream(myByteArray);

                    //    logger.Info("Start Process to remove older Csv File from S3 bucket");
                    //    await AWSHelper.RemoveFileFromS3(FileName);

                    //    logger.Info("Start Process to upload new Csv File");
                    //    AWSHelper.UploadFileToS3(ms, null, FileName);


                    //    //byte[] myfile = await new WebClient().DownloadDataTaskAsync(new Uri(AWSHelper.GetPresignedFileURL(FileName)));
                    //    //return File(myfile, "image/png", FileName);
                    //}
                }
                return Ok(new { });
            }
            catch (Exception ex)
            {
                logger.Error("GenerateChannelPredictionFile: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        private DataTable ConvertListToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        private object ConvertDataTableToCsv(DataTable dtDataTable)
        {
            StringBuilder sb = new StringBuilder();
            //headers    
            for (int i = 0; i < dtDataTable.Columns.Count; i++)
            {
                sb.Append(dtDataTable.Columns[i]);
                if (i < dtDataTable.Columns.Count - 1)
                {
                    sb.Append(",");
                }
            }
            sb.Append(Environment.NewLine);
            //Rows   
            foreach (DataRow dr in dtDataTable.Rows)
            {
                for (int i = 0; i < dtDataTable.Columns.Count; i++)
                {
                    if (!Convert.IsDBNull(dr[i]))
                    {
                        string value = dr[i].ToString();
                        value = value.Replace("\n", "").Replace("\r", "").Replace(",", ";");
                        if (value.Length > 32767)
                        {
                            value = value.Substring(0, 32750);
                        }
                        sb.Append(value);
                    }
                    if (i < dtDataTable.Columns.Count - 1)
                    {
                        sb.Append(",");
                    }
                }
                sb.Append(Environment.NewLine);
            }
            return sb;
        }
    }
}