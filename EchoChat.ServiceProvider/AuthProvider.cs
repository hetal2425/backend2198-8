﻿using EchoChat.Models;
using EchoChat.Repository;
using System;

namespace EchoChat.ServiceProvider
{
    public class AuthProvider : IAuthProvider
    {
        IAuthRepository authRepository;
        public AuthProvider(IAuthRepository oAuthRepository)
        {
            authRepository = oAuthRepository;
        }

        public UserData CheckUserLogin(LoginRequest user)
        {
            return authRepository.CheckUserLogin(user);
        }
    }
}
