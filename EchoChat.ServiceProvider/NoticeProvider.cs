﻿using EchoChat.Models;
using EchoChat.Repository;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EchoChat.ServiceProvider
{
    public class NoticeProvider : INoticeProvider
    {
        INoticeRepository noticeRepository;
        public NoticeProvider(INoticeRepository oNoticeRepository)
        {
            noticeRepository = oNoticeRepository;
        }

        public bool SaveNoticeOutput(int noticeID, int clientID, ConcurrentBag<ChannelOutput> output)
        {
            try
            {
                return noticeRepository.SaveNoticeOutput(noticeID, clientID, output);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DeletedNotice> DeleteNotice(int noticeID)
        {
            try
            {
                return noticeRepository.DeleteNotice(noticeID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public NoticeDetailDTO GetNoticeDetailsByNoticeID(int noticeID)
        {
            try
            {
                return noticeRepository.GetNoticeDetailsByNoticeID(noticeID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public NoticeSmallDataModel GetNoticeListByGroupID(int groupID, int parentID, int pageNo, int pageSize, Guid userID, string freeText, int timeZoneOffset)
        {
            try
            {
                if (parentID == 0)
                {
                    return noticeRepository.GetNoticeListByGroupID(groupID, pageNo, pageSize, userID, freeText, timeZoneOffset);
                }
                else
                {
                    return noticeRepository.GetNoticeRepliesListByGroupID(parentID, pageNo, pageSize, userID, freeText, timeZoneOffset);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SaveNotice(List<UserChannelInfo> userChannelInfos, int noticeID, int groupID, Guid createdBy)
        {
            try
            {
                return noticeRepository.SaveNotice(userChannelInfos, noticeID, groupID, createdBy);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SaveNoticeDetails(NoticeDetailDTO noticeDetails)
        {
            try
            {
                return noticeRepository.SaveNoticeDetails(noticeDetails);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SeenNotice(int noticeID, Guid userID, string type)
        {
            try
            {
                noticeRepository.SeenNotice(noticeID, userID, type);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ScheduledNotices> GetScheduledNotices(DateTime fromDate, DateTime toDate, int timeZoneOffset)
        {
            try
            {
                return noticeRepository.GetScheduledNotices(fromDate, toDate, timeZoneOffset);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool SaveIntegrationNotice(List<UserChannelInfo> userChannelInfos, int noticeID, int groupID, Guid createdBy)
        {
            try
            {
                return noticeRepository.SaveIntegrationNotice(userChannelInfos, noticeID, groupID, createdBy);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SaveIntegrationNoticeDetails(NoticeDetailDTO noticeDetails)
        {
            try
            {
                return noticeRepository.SaveIntegrationNoticeDetails(noticeDetails);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SavePrivateNotice(List<UserChannelInfo> userChannelInfos, int noticeID, int groupID, Guid createdBy)
        {
            try
            {
                return noticeRepository.SavePrivateNotice(userChannelInfos, noticeID, groupID, createdBy);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SavePrivateNoticeDetails(NoticeDetailDTO noticeDetails)
        {
            try
            {
                return noticeRepository.SavePrivateNoticeDetails(noticeDetails);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<string> GetConfigWhatsAppTemplate()
        {
            try
            {
                return noticeRepository.GetConfigWhatsAppTemplate();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<string> GetConfigEmailTemplate()
        {
            try
            {
                return noticeRepository.GetConfigEmailTemplate();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public HubNotice GetNoticeDetailsForHub(int noticeID)
        {
            try
            {
                return noticeRepository.GetNoticeDetailsForHub(noticeID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}