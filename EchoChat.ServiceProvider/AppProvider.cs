﻿using EchoChat.Models;
using EchoChat.Repository;
using System;

namespace EchoChat.ServiceProvider
{
    public class AppProvider : IAppProvider
    {
        IAppRepository appRepository;
        public AppProvider(IAppRepository oAppRepository)
        {
            appRepository = oAppRepository;
        }

        public string GetAppVersion(string platform)
        {
            try
            {
                return appRepository.GetAppVersion(platform);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SaveAppVersion(AppVersionDTO appRequest)
        {
            try
            {
                return appRepository.SaveAppVersion(appRequest);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
