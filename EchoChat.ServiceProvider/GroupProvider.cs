﻿using EchoChat.Models;
using EchoChat.Repository;
using System;
using System.Collections.Generic;

namespace EchoChat.ServiceProvider
{
    public class GroupProvider : IGroupProvider
    {
        IGroupRepository groupRepository;
        public GroupProvider(IGroupRepository oGroupRepository)
        {
            groupRepository = oGroupRepository;
        }

        public GroupDTO GetGroupByID(int groupID)
        {
            try
            {
                return groupRepository.GetGroupByID(groupID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public GroupDTO CreateUpdateGroup(GroupDTO group)
        {
            try
            {
                return groupRepository.CreateUpdateGroup(group);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool CreateUpdateList(ListDTO list)
        {
            try
            {
                return groupRepository.CreateUpdateList(list);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteGroup(int groupID)
        {
            try
            {
                return groupRepository.DeleteGroup(groupID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteList(int listID)
        {
            try
            {
                return groupRepository.DeleteList(listID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<GroupDTO> GetAllGroupsByClientID(int clientID)
        {
            try
            {
                return groupRepository.GetAllGroupsByClientID(clientID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ListDTO> GetAllListByClientID(int clientID, string freeText, int timeZoneOffset)
        {
            try
            {
                return groupRepository.GetAllListByClientID(clientID, freeText, timeZoneOffset);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ListSmallDTO> GetAllListByGroupID(int groupID)
        {
            try
            {
                return groupRepository.GetAllListByGroupID(groupID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ListOrGroupWiseUsers GetMembersByListID(int listID, int pageSize, int PageNo, string freeText, int timeZoneOffset)
        {
            try
            {
                return groupRepository.GetMembersByListID(listID, pageSize, PageNo, freeText, timeZoneOffset);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UserChannelDataWithGroup GetGroupMappingUsers(string groupID, int parentID, string noticeDetails)
        {
            try
            {
                return groupRepository.GetGroupMappingUsers(groupID, parentID, noticeDetails);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UserChannelInfo> GetUsersForPendingNotice(int noticeID, bool isSMS, bool isEmail, bool isWA)
        {
            try
            {
                return groupRepository.GetUsersForPendingNotice(noticeID, isSMS, isEmail, isWA);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ListOrGroupWiseUsers GetMembersByGroupID(int groupID, int pageSize, int PageNo, string freeText, int timeZoneOffset)
        {
            try
            {
                return groupRepository.GetMembersByGroupID(groupID, pageSize, PageNo, freeText, timeZoneOffset);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UserGroupModel GetUserGroups(Guid userID, string freeText, int pageSize, int PageNo, int timeZoneOffset)
        {
            try
            {
                return groupRepository.GetUserGroups(userID, freeText, pageSize, PageNo, timeZoneOffset);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SetGroupListMapping(GroupListMapping mapping)
        {
            try
            {
                return groupRepository.SetGroupListMapping(mapping);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool CheckGroupAlreadyExists(int groupID, string name)
        {
            try
            {
                return groupRepository.CheckGroupAlreadyExists(groupID, name);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool CheckListAlreadyExists(int listID, string name)
        {
            try
            {
                return groupRepository.CheckListAlreadyExists(listID, name);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public GroupDTO CreateInteGrationGroup(GroupDTO group, List<Guid> lstGUsers)
        {
            try
            {
                return groupRepository.CreateInteGrationGroup(group, lstGUsers);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int CheckIntegrationGroupAlreadyExists(string name)
        {
            try
            {
                return groupRepository.CheckIntegrationGroupAlreadyExists(name);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public GroupDTO CreatePrivateGroup(GroupDTO group, List<Guid> lstGUsers)
        {
            try
            {
                return groupRepository.CreatePrivateGroup(group, lstGUsers);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int CheckPrivateGroupAlreadyExists(string name, string mobileNo)
        {
            try
            {
                return groupRepository.CheckPrivateGroupAlreadyExists(name, mobileNo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<DNDUserModel> GetAllMembersByListID(int listID)
        {
            try
            {
                return groupRepository.GetAllMembersByListID(listID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool SaveDNDStatus(Guid userID, List<DNDStatusModel> listDNDStatus)
        {
            try
            {
                return groupRepository.SaveDNDStatus(userID, listDNDStatus);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DNDReportDetailsModel GetDNDReoprt(int clientID, int pageSize, int pageNo, string freeText, int TimeZoneOffset)
        {
            try
            {
                return groupRepository.GetDNDReoprt(clientID, pageSize, pageNo, freeText, TimeZoneOffset);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
