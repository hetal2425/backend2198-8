﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EchoChat.ChannelModels
{// Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class ParameterValues
    {
        public string _0 { get; set; }
        public string _1 { get; set; }
    }
    public class TemplateKarix
    {
        public string templateId { get; set; }
        public ParameterValues parameterValues { get; set; }
    }
    public class Content
    {
        public bool preview_url { get; set; }
        public string type { get; set; }
        public TemplateKarix template { get; set; }       
        public bool shorten_url { get; set; }
        public MediaTemplate mediaTemplate { get; set; }
    }
    public class Recipient
    {
        public string to { get; set; }
        public string recipient_type { get; set; }
    }

    public class Sender
    {
        public string from { get; set; }
    }
    public class Preferences
    {
        public string webHookDNId { get; set; }
    }
    public class TextMessage
    {
        public string channel { get; set; }
        public Content content { get; set; }
        public Recipient recipient { get; set; }
        public Sender sender { get; set; }
        public Preferences preferences { get; set; }       
    }
    public class MetaData
    {
        public string version { get; set; }
    }

    public class WhatsAPPKarixMessage
    {
        public TextMessage message { get; set; }
        public MetaData metaData { get; set; }
    }

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Media
    {
        public string type { get; set; }
        public string url { get; set; }
        public string fileName { get; set; }        
    }

    public class BodyParameterValues
    {
        public string _0 { get; set; }
        public string _1 { get; set; }
    }
    
    public class MediaTemplate
    {
        public string templateId { get; set; }
        public Media media { get; set; }
        public BodyParameterValues bodyParameterValues { get; set; }
    }
   
    public class TemplateModelKarix
    {
        public string TemplateId { get; set; }
        public string ContentType { get; set; }
        public bool IsGeneric { get; set; }
        public bool IsWithoutParams { get; set; }
        public string MediaType { get; set; }
        public string ParameterCount { get; set; }

    }
    public class WAResponseKarix
    {
        public string statusCode { get; set; }
        public string statusDesc { get; set; }
        public string mid { get; set; }
    }

}