﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using EchoChat.Channel.Factory;
using EchoChat.Constants;
using EchoChat.Models;
using Newtonsoft.Json;
using log4net;
using System.Linq;
using System.Net.Http;

namespace EchoChat.Channel
{
    public class SlackChannel : BaseChannel
    {
        private ILog logger = LogManager.GetLogger(typeof(SlackChannel));
        public string channelToken { get; set; }

        public string messagePostUrl { get; set; }
        public string userId { get; set; }
        public SlackChannel()
        {
        }

        public override Task Process()
        {

            try
            {
                string msg = Essentials.messageDetails.Message;

                List<ChannelLevelSettings> appSettings = Essentials.settings.Where(a => a.Key.Equals(SettingMaster.SlackAppSettings)).ToList();

                foreach (ChannelLevelSettings cls in appSettings)
                {
                    messagePostUrl = cls.Value;
                    channelToken = cls.Value1;
                }
                userId = (JsonConvert.DeserializeObject<MessageDetailNotice>(Essentials.messageDetails.CustomDetails_3)).notice.UserID.ToString();
                if (!string.IsNullOrEmpty(messagePostUrl))
                {
                    foreach (var ucd in Essentials.userChannelDatas)
                    {
                        if (!string.IsNullOrWhiteSpace(ucd.SlackID))
                            Send(Essentials.messageDetails.Message, ucd.DisplayName, ucd.SlackID);
                    }
                }
                return Task.Delay(0);
            }
            catch (Exception ex)
            {
                logger.Error("Process: " + ex.Message);
                throw ex;
            }
        }

        public void Send(string text, string username = null, string channel = null)
        {
            try
            {
                string payloadJson = JsonConvert.SerializeObject(new
                {
                    channel = channel,
                    username = username,
                    text = text,
                    as_user = true
                });

                var content = new StringContent(payloadJson.ToString(), Encoding.UTF8, "application/json");
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", channelToken);
                var result = client.PostAsync(messagePostUrl, content).Result;
                if (result.IsSuccessStatusCode)
                {
                    var resultString = result.Content.ReadAsStringAsync().Result;
                    var responseObject = JsonConvert.DeserializeObject<SlackPostMessageResponse>(resultString);
                    if (responseObject.ok)
                    {
                        Essentials.PushInOutput(ChannelMaster.SlackChannel, userId, responseObject.ts, string.Empty);
                    }
                    else
                    {
                        Essentials.PushInOutput(ChannelMaster.SlackChannel, userId, "response not ok", string.Empty);
                    }
                }
                else
                {
                    if (result.Content != null)
                    {
                        var resultString = result.Content.ReadAsStringAsync().Result;
                        Essentials.PushInOutput(ChannelMaster.SlackChannel, userId, (resultString != null && resultString.Length > 100 ? resultString[..100] : resultString), string.Empty);
                    }
                    else
                    {
                        Essentials.PushInOutput(ChannelMaster.SlackChannel, userId, "content is empty", string.Empty);
                    }
                }
            }
            catch (Exception ex)
            {
                Essentials.PushInOutput(ChannelMaster.SlackChannel, userId, string.Empty, ex.Message, false);
                logger.Error("SendSlack: " + ex.Message);
                throw ex;
            }
        }
    }
    public class SlackPostMessageResponse
    {
        public bool ok { get; set; }
        public string ts { get; set; }
    }
    public class MessageDetailNotice
    {
        public NoticeSignalRData notice { get; set; }
    }
}